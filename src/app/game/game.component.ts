import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, Input } from '@angular/core';

import * as PIXI from 'pixi.js';
import { fromEvent, interval } from 'rxjs';
import { webSocket } from 'rxjs/webSocket';
import { tap } from 'rxjs/operators';

export class SpaceshipState {
  x = 400; 
  y = 300;
  vx = 0; 
  vy = 0;
  rotation = 0;
  engineClass = 1;
  credits = 1000;
  fuel = 50000;
  fuelCapacity = 50000;
  cargo: { name: string, origin: { x: number, y: number }}[]  = [];
  cargoCapacity = 4;
  shipStatus = null;
  orbiting: { x: number, y: number } = null;
  nick = null;
  crashed = false;
  speed = 0
};

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit, AfterViewInit {

  @Input() nick: string;

  @ViewChild('canvasContainer') canvasContainer: ElementRef;

  pixiApp = new PIXI.Application();

  spaceshipState = new SpaceshipState();
  
  gameState: { ships: SpaceshipState[] } = { ships: [] };

  readonly SPACE_SECTOR_SIZE = 10000;

  constructor() { }

  ngOnInit() {
    const { pixiApp, spaceshipState, gameState, canvasContainer } = this;
    const { SPACE_SECTOR_SIZE } = this;

    const socket = webSocket('wss://star-birds.herokuapp.com');
    // const socket = webSocket('ws://localhost:81');

    socket.pipe(
      tap((ships: SpaceshipState[]) => {
        this.gameState.ships = ships;
      })
    ).subscribe();

    interval(1000/30).pipe(
      tap(() => socket.next(this.spaceshipState))
    ).subscribe();

    spaceshipState.nick = this.nick;

    socket.next(spaceshipState);

    PIXI.Loader.shared
      .add([
        'assets/spaceship.png',
        'assets/explosion.png',
        'assets/planet.png',
      ])
      .load(setup);

    let spaceship: PIXI.Container;
    let spaceships: { nick: string, sprite: PIXI.Container }[] = [];
    let space: PIXI.Container;
    let stars: PIXI.Container;
    let planets: PIXI.Container;

    let currentSector;

    function setup() {
      space = new PIXI.Container();
      pixiApp.stage.addChild(space);

      stars = new PIXI.Container();
      for (let index = 0; index < 5000; index++) {
        const star = new PIXI.Graphics();
        const size = Math.random() * 5;
        star.beginFill(0xFFFFFF);
        star.drawStar(0, 0, 4, size);
        star.endFill();
        star.position.set(
          SPACE_SECTOR_SIZE * Math.random(),
          SPACE_SECTOR_SIZE * Math.random()
        );
        star.rotation = 2 * Math.PI * Math.random();
        stars.addChild(star);
      }
      space.addChild(stars);

      planets = new PIXI.Container();
      renderSpaceSector(0, 0);
      space.addChild(planets);

      spaceship = addSpaceship(window.innerWidth / 2, window.innerHeight / 2, spaceshipState.nick);

      connectKeyboard();

      pixiApp.ticker.add(delta => gameLoop(delta));
    }

    function gameLoop(delta) {
      const { rotation, speed, engineClass, fuel, crashed } = spaceshipState;
      if (crashed) return;
      const engines = [
        { engineClass: 1, speed: 0.25, consumption: 0.01 }
      ]
      const engine = engines.find(e => e.engineClass === engineClass);
      spaceship.children[0].rotation = rotation;
      const vy = -Math.cos(rotation) * speed;
      const vx = Math.sin(rotation) * speed;
      const xMove = fuel > 0 ? vx * (1 + delta) * engine.speed : 0;
      const yMove = fuel > 0 ? vy * (1 + delta) * engine.speed : 0;
      if (xMove || yMove) {
        space.x -= xMove;
        space.y -= yMove;
        stars.x += .5 * xMove;
        stars.y += .5 * yMove;
        spaceship.x += xMove;
        spaceship.y += yMove;
        spaceship.children[0].rotation = rotation;
        spaceshipState.fuel -= fuel > 0 ? Math.abs(speed) * engine.consumption : 0;
        spaceshipState.fuel = Math.max(0, spaceshipState.fuel);
        spaceshipState.x = -Math.round(space.position.x) + window.innerWidth / 2;
        spaceshipState.y = -Math.round(space.position.y) + window.innerHeight / 2;
        spaceshipState.shipStatus = null;
        const distancesToPlanets = planets.children
          .map(p => ({ planet: p, distance: Math.sqrt(Math.pow(p.x - spaceship.x, 2) + Math.pow(p.y - spaceship.y, 2)) }));
        if (distancesToPlanets.some(d => d.distance < 1000)) {
          const shortestDistance = Math.round(Math.min(...distancesToPlanets.map(d => d.distance)));
          spaceshipState.shipStatus = `Computer Reading: Approaching Planet! (${shortestDistance})`;
        }
        spaceshipState.orbiting = null;
        const planet = distancesToPlanets.find(p => p.planet.hitArea.contains(spaceship.position.x, spaceship.y));
        if (planet) {
          spaceshipState.orbiting = { x: Math.round(planet.planet.x), y: Math.round(planet.planet.y) };
          spaceshipState.shipStatus = `Orbiting Planet! Welcome Commander.`;
          if (spaceshipState.speed > 2) {
            (spaceship.children[0] as PIXI.Sprite).texture = PIXI.Loader.shared.resources['assets/explosion.png'].texture;
            spaceshipState.crashed = true;
            spaceshipState.shipStatus = 'CRASHED! Always dock and undock at the safe-orbiting speed.'
          }
        }
        const currentSectorOrigin = new PIXI.Point(Math.floor(spaceship.x / SPACE_SECTOR_SIZE) * SPACE_SECTOR_SIZE, Math.floor(spaceship.y / SPACE_SECTOR_SIZE) * SPACE_SECTOR_SIZE);
        if (currentSector.x !== currentSectorOrigin.x || currentSector.y !== currentSectorOrigin.y) {
          renderSpaceSector(currentSectorOrigin.x, currentSectorOrigin.y);
        }
      }
      gameState.ships.filter(s => s.nick !== spaceshipState.nick).forEach(s => {
        let shipSprite = spaceships.find(sprite => sprite.nick === s.nick);
        if (!shipSprite) {
          shipSprite = { nick: s.nick, sprite: addSpaceship(s.x, s.y, s.nick) };
          spaceships.push(shipSprite);
        }
        shipSprite.sprite.x = s.x;
        shipSprite.sprite.y = s.y;
        shipSprite.sprite.children[0].rotation = s.rotation;
      });
    }

    function renderSpaceSector(x, y) {
      currentSector = { x, y };
      stars.position.set(x, y);
      planets.children.forEach(p => p.destroy());
      const generator = new SpaceProceduralGenerator(x + y);
      for (let index = 0; index < 20; index++) {
        const planet = new PIXI.Sprite(
          PIXI.Loader.shared.resources['assets/planet.png'].texture
        );
        planet.anchor.set(.5);
        const size = generator.random() + 0.5;
        planet.width = 128 * size;
        planet.height = 128 * size;
        planet.position.set(
          x + SPACE_SECTOR_SIZE * generator.random(),
          y + SPACE_SECTOR_SIZE * generator.random()
        );
        planet.hitArea = new PIXI.Circle(planet.position.x, planet.position.y, planet.width/2);
        planet.rotation = 2 * Math.PI * generator.random();
        planets.addChild(planet);
      }
    }

    function addSpaceship(x, y, nick = null) {
      const spaceshipContainer = new PIXI.Container();
      const spaceship = new PIXI.Sprite(
        PIXI.Loader.shared.resources['assets/spaceship.png'].texture
      );
      spaceship.width = 64;
      spaceship.height = 64;
      spaceship.anchor.set(.5);
      const nickText = new PIXI.Text(nick.slice(0, 10), { fontFamily: 'sans-serif', fill: '#ffffff', fontSize: 12, fontWeight: 'bold' });
      nickText.anchor.set(.5);
      nickText.y = 64;
      const box = new PIXI.Graphics();
      box.lineStyle(2, 0xdddddd, .5);
      box.beginFill(0x000000, .5);
      box.drawRoundedRect(0, 0, 80, 24, 4);
      box.endFill();
      box.x = -40;
      box.y = 53;
      spaceshipContainer.addChild(spaceship);
      spaceshipContainer.addChild(box);
      spaceshipContainer.addChild(nickText);
      spaceshipContainer.position.set(x, y);
      space.addChild(spaceshipContainer);
      return spaceshipContainer;
    }

    function connectKeyboard() {
      fromEvent(window, 'keydown').subscribe((e: KeyboardEvent) => {
        switch (e.key) {
          case 'd': case 'ArrowRight': spaceshipState.rotation += .125 * Math.PI; break;
          case 'a': case 'ArrowLeft': spaceshipState.rotation -= .125 * Math.PI; break;
          case 'w': case 'ArrowUp': spaceshipState.speed += 1; break;
          case 's': case 'ArrowDown': spaceshipState.speed -= 1; break;
        }
        spaceshipState.speed = Math.min(spaceshipState.speed, 20);
        spaceshipState.speed = Math.max(spaceshipState.speed, -20);
      });
    }
  }

  safeDockingSpeed() {
    return Math.abs(this.spaceshipState.speed) < 3 && this.spaceshipState.speed !== 0; 
  }

  restart() {
    window.location.reload();
  }

  ngAfterViewInit() {
    this.pixiApp.resizeTo = this.canvasContainer.nativeElement;
    this.canvasContainer.nativeElement.append(this.pixiApp.view);
  }
}

class SpaceProceduralGenerator {
  constructor(public seed) { }
  random() {
    var x = Math.sin(this.seed++) * 10000;
    return x - Math.floor(x);
  }
}
