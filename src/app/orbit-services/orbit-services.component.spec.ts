import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrbitServicesComponent } from './orbit-services.component';

describe('OrbitServicesComponent', () => {
  let component: OrbitServicesComponent;
  let fixture: ComponentFixture<OrbitServicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrbitServicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrbitServicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
