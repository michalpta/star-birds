import { Component, OnInit, Input } from '@angular/core';
import { SpaceshipState } from '../game/game.component';

@Component({
  selector: 'app-orbit-services',
  templateUrl: './orbit-services.component.html',
  styleUrls: ['./orbit-services.component.scss']
})
export class OrbitServicesComponent implements OnInit {

  @Input() spaceshipState: SpaceshipState;

  activeService: string;

  constructor() { }

  ngOnInit(): void {
  }

  refuel(amount) {
    const { credits } = this.spaceshipState;
    const price = 1;
    amount = Math.min((credits / price), amount, this.spaceshipState.fuelCapacity - this.spaceshipState.fuel); 
    this.spaceshipState.fuel += amount;
    this.spaceshipState.credits -= amount * price;
  }

  buy(name, amount) {
    const { credits } = this.spaceshipState;
    const price = 50;
    amount = Math.min((credits / price), amount, this.spaceshipState.cargoCapacity - this.spaceshipState.cargo.length);
    const cargo = new Array(amount).fill(null).map(() => ({ name, origin: this.spaceshipState.orbiting })); 
    this.spaceshipState.cargo.push(...cargo);
    this.spaceshipState.credits -= amount * price;
  }
  sell(amount) {
    const price = 100;
    let itemsToSell = this.spaceshipState.cargo
      .filter(i => i.origin.x !== this.spaceshipState.orbiting.x && i.origin.y !== this.spaceshipState.orbiting.y);
    itemsToSell = itemsToSell
      .slice(0, amount);
    this.spaceshipState.cargo = this.spaceshipState.cargo.filter(i => !itemsToSell.includes(i));
    this.spaceshipState.credits += itemsToSell.length * price;
  }

}
