import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  music: HTMLAudioElement;
  introVisible = false;
  gameVisible = false;
  nick = 'Michal';

  constructor() {
    const music = new Audio('assets/background-music.mp3');
    music.loop = true;
    music.currentTime = 35;
    this.music = music;
  }  
  
  startIntro() {
    this.music.play();
    this.introVisible = true;
  }

  startGame() {
    this.nick = prompt('What is your name Commander?');
    if (this.nick) {
      this.introVisible = false;
      this.gameVisible = true;
    }
  }

}
